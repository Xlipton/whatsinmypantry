package com.xlipton.whatsinmypantry

import android.content.res.Resources
import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.core.text.HtmlCompat
import com.xlipton.whatsinmypantry.database.entities.Category
import com.xlipton.whatsinmypantry.database.entities.CategoryWithPantryItems
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import com.xlipton.whatsinmypantry.database.entities.ShoppingListItem


fun formatItems(pantryItems: List<PantryItem>): Spanned {
    val sb = StringBuilder()
    sb.apply {
        pantryItems.forEach {
            append("<b>name: </b>")
            append(it.itemName)
            append("<br>")
            append("<b>category: </b>")
            append(it.categoryName)
            append(", <b>amount: </b>")
            append(it.amount)
            append("<b>${it.unit}</b>")
            append(", <b>comment: </b>")
            append(it.comment)
            append("<br>")
            append("<br>")
        }
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_LEGACY)
    } else {
        return HtmlCompat.fromHtml(sb.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}
fun formatCategories(pantryItems: List<Category>): Spanned {
    val sb = StringBuilder()
    sb.apply {
        pantryItems.forEach {
            append("<br>")
            append("<br>")
            append("<b>name: </b>")
            append(it.categoryName)
        }
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_LEGACY)
    } else {
        return HtmlCompat.fromHtml(sb.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}

fun formatShoppingListItems(shoppingListItems: List<ShoppingListItem>): Spanned {
    val sb = StringBuilder()
    sb.apply {
        shoppingListItems.forEach {
            append("<b>${it.name}</b>")
            append(it.amount)
            append("<b>${it.unit}</b>")
            append("<br>")
            append("<br>")
        }
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_LEGACY)
    } else {
        return HtmlCompat.fromHtml(sb.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}
/*
fun formatOneShoppingListItem(shoppingListItem: ShoppingListItem): String {
    val sb = StringBuilder()
    sb.apply {
        shoppingListItems.forEach {
            append("<b>${it.name}</b>")
            append(it.amount)
            append("<b>${it.unit}</b>")
            append("<br>")
            append("<br>")
        }
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_LEGACY)
    } else {
        return HtmlCompat.fromHtml(sb.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}
*/
