package com.xlipton.whatsinmypantry.newpantryitem

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.xlipton.whatsinmypantry.database.MyPantryDatabaseDao
import com.xlipton.whatsinmypantry.database.entities.Category
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import kotlinx.coroutines.*

class NewPantryItemViewModel(val database: MyPantryDatabaseDao) : ViewModel() {
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var name: String = ""
    var category: String = ""
    var amount: String = ""
    var unit: String = ""
    var tags: String = ""
    var comment: String = ""

    //val categories = database.getAllCategoryLive()
    val nameOfCategoriesLive = database.getAllCategoryNameLive()

    private val _eventSave = MutableLiveData<Boolean>()
    val eventSave: LiveData<Boolean>
        get() = _eventSave

    private val _eventPopUp = MutableLiveData<String>()
    val eventPopUp: LiveData<String>
        get() = _eventPopUp


    fun onSave() {
        Log.i("Save", "Save is happening with name: $name")
        if (name == ""){
            _eventPopUp.value = "Name can't be empty!"
        } else{
            _eventSave.value = true
            uiScope.launch {
                withContext(Dispatchers.IO) {
                    if (!database.getAllCategoryName().contains(category)) database.insertCategory(Category(category))
                    val item = PantryItem(name, if (!category.isNullOrEmpty()) { category } else { "uncategorised" })
                    item.amount = if (amount.isNotEmpty()) {
                        amount.toLong()
                    } else {
                        0L
                    }
                    item.unit = unit
                    item.tags = tags
                    item.comment = comment
                    database.insertPantryItem(item)
                }
            }
        }
    }

    fun onSaved() {
        _eventSave.value = false
    }
}