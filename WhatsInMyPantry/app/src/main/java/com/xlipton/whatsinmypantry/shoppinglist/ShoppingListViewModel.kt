package com.xlipton.whatsinmypantry.shoppinglist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.xlipton.whatsinmypantry.database.MyPantryDatabaseDao
import com.xlipton.whatsinmypantry.database.entities.Category
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import com.xlipton.whatsinmypantry.database.entities.ShoppingListItem
import kotlinx.coroutines.*

class ShoppingListViewModel (val database: MyPantryDatabaseDao, application: Application) : AndroidViewModel(application) {

    val shoppingList = database.getAllShoppingListItemLive()

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var name: String = ""
    var amount: String = ""
    var unit: String = ""

    //val categories = database.getAllCategoryLive()
    //val nameOfCategoriesLive = database.getAllCategoryNameLive()

    private val _eventAdd = MutableLiveData<Boolean>()
    val eventSave: LiveData<Boolean>
        get() = _eventAdd

    private val _eventPopUp = MutableLiveData<String>()
    val eventPopUp: LiveData<String>
        get() = _eventPopUp


    fun onChangeAddToPantryField(itemId: Long){
        uiScope.launch {
            withContext(Dispatchers.IO) {
                val item = database.getOneShoppingListItem(itemId)
                item.addToPantry = !item.addToPantry
                database.updateShoppingListItem(item)
            }
        }
    }

    fun onDeleteFromDatabase(itemId: Long){
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.deleteOneShoppingListItem(itemId)
            }
        }
        _eventPopUp.value = "Delete was successful!"
    }

    fun onAddToPantry(){
        uiScope.launch {
            withContext(Dispatchers.IO) {
                val shoppingListItems = database.getAllAddToPantryShoppingListItem()
                shoppingListItems.forEach {
                    val pantryItem = PantryItem(it.name, "uncategorised")
                    if (!database.getAllCategoryName().contains("uncategorised")) database.insertCategory(Category("uncategorised"))

                    if (database.getAllPantryItemName().contains(it.name)){
                        val existingPantryItem = database.getOnePantryItem(it.name)
                        if(existingPantryItem.unit == it.unit){
                            existingPantryItem.amount += it.amount
                        } else{
                            existingPantryItem.amount = it.amount
                            existingPantryItem.unit = it.unit
                        }
                        existingPantryItem.accessCounter += 1
                        database.updatePantryItem(existingPantryItem)

                    } else{
                        pantryItem.amount = it.amount
                        pantryItem.unit = it.unit
                        database.insertPantryItem(pantryItem)
                    }

                    database.deleteOneShoppingListItem(it.itemId)
                }
            }
        }
        _eventPopUp.value = "All checked items added to your Pantry!"
    }

    fun onAddNewShoppingListItem(){
        if (name == ""){
            _eventPopUp.value = "Name can't be empty!"
        } else{
            _eventAdd.value = true
        }
    }

    fun insertShoppingListItem() {
        val item = ShoppingListItem(name.toLowerCase())
        _eventAdd.value = true
        uiScope.launch {
            withContext(Dispatchers.IO) {
                item.amount = if (amount.isNotEmpty()) {
                    amount.toLong()
                } else {
                    0L
                }
                item.unit = unit
                database.insertShoppingListItem(item)
            }
        }
    }

    fun onAdded() {
        _eventAdd.value = false
    }


}