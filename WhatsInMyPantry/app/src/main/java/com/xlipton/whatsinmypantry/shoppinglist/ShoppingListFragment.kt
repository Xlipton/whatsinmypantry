package com.xlipton.whatsinmypantry.shoppinglist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.xlipton.whatsinmypantry.R
import com.xlipton.whatsinmypantry.database.MyPantryDatabase
import com.xlipton.whatsinmypantry.databinding.FragmentShoppingListBinding

class ShoppingListFragment : Fragment(), AdapterView.OnItemSelectedListener {
    private lateinit var viewModel: ShoppingListViewModel
    private lateinit var unit: String
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding: FragmentShoppingListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shopping_list, container, false)

        val application = requireNotNull(this.activity).application

        val dataSource = MyPantryDatabase.getInstance(application).myPantryDatabaseDao

        val viewModelFactory = ShoppingListViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this,viewModelFactory).get(ShoppingListViewModel::class.java)

        binding.lifecycleOwner = this
        binding.shoppingListViewModel = viewModel

        // Unit Spinner
        val unitSpinner: Spinner = binding.unitSpinner
        unitSpinner.onItemSelectedListener = this

        activity?.applicationContext?.let { ArrayAdapter.createFromResource(it, R.array.unit_array, android.R.layout.simple_spinner_item).also{
                arrayAdapter -> arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            unitSpinner.adapter = arrayAdapter
        }}
        // Unit Spinner END

        // RecyclerView
        val adapter = ShoppingListItemAdapter(ShoppingListItemListener({viewModel.onChangeAddToPantryField(it)},{viewModel.onDeleteFromDatabase(it)}))
        binding.shoppingListItemList.adapter = adapter
        viewModel.shoppingList.observe(viewLifecycleOwner, Observer {
            it?.let{
                adapter.submitList(it)
                binding.shoppingListAddCheckedItemsButton.hide()
                it.forEach{ item ->
                    if(item.addToPantry) binding.shoppingListAddCheckedItemsButton.show()
                }
            }
        })

        viewManager = LinearLayoutManager(activity)
        binding.shoppingListItemList.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        // RecyclerView END

        // Add button event observer
        viewModel.eventSave.observe(viewLifecycleOwner, Observer {
            if(it){
                viewModel.unit = unit
                viewModel.insertShoppingListItem()
                viewModel.onAdded()
            }
        })

        // Pop up event observer
        viewModel.eventPopUp.observe(viewLifecycleOwner, Observer {
            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        })

        // Navigate to Pantry Fragment
        binding.pantryButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_shoppingListFragment_to_pantryFragment))

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        unit = parent?.getItemAtPosition(position).toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        unit = parent?.firstVisiblePosition.toString()
    }
}