package com.xlipton.whatsinmypantry.pantry

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.xlipton.whatsinmypantry.database.*
import com.xlipton.whatsinmypantry.database.entities.Category
import com.xlipton.whatsinmypantry.database.entities.CategoryWithPantryItems
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import com.xlipton.whatsinmypantry.databinding.FragmentPantryItemBinding
import com.xlipton.whatsinmypantry.formatItems
import kotlinx.coroutines.*

class PantryViewModel(val database: MyPantryDatabaseDao, application: Application) : AndroidViewModel(application){
    private var viewModelJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var _navigationToPantryItem = MutableLiveData<PantryItem>()
    val navigationToPantryItems: LiveData<PantryItem>
    get() = _navigationToPantryItem

    val categories = database.getAllCategoryLive()

    //val categories = database.getAllCategoryLive()
    val nameOfCategoriesLive = database.getAllCategoryNameLive()

    var name: String = "asd"
    var category: String = ""
    var amount: String = ""
    var unit: String = ""
    var tags: String = ""
    var comment: String = ""

    private val _eventPopUp = MutableLiveData<String>()
    val eventPopUp: LiveData<String>
        get() = _eventPopUp

    val pantryItems = database.getPantryItemsWhereCategoryIsSelectedLive()

    fun onChipClick(chipText: String){
        uiScope.launch {
            withContext(Dispatchers.IO){
                var category = database.getOneCategory(chipText)
                category.isSelected = !category.isSelected
                database.updateCategory(category)
            }
        }
        //_eventPopUp.value = "Chip Click registered with ${chipText}!"
    }

    fun onStar(itemName: String){
        uiScope.launch {
            withContext(Dispatchers.IO){
                var item = database.getOnePantryItem(itemName)
                item.isFavourite = !item.isFavourite
                database.updatePantryItem(item)
            }
        }
    }


    fun onPantryItemCLick(itemName: String){
        uiScope.launch {
            withContext(Dispatchers.IO){
                val item = database.getOnePantryItem(itemName)
                _navigationToPantryItem.value = item
                item.accessCounter += 1
            }
        }
    }

    fun doneToPantryItemNavigating(){
        _navigationToPantryItem.value = null
    }


    // PantryItemViewModel part

    fun onLoad(itemName: String,  binding: FragmentPantryItemBinding){
        Log.i("elegem van", itemName)
        uiScope.launch {
            withContext(Dispatchers.IO) {
                val item = database.getOnePantryItem(itemName)
                name = item.itemName
                amount = item.amount.toString()
                unit = item.unit
                category = item.categoryName
                comment = item.comment
                tags = item.tags
                binding.invalidateAll()
            }
        }
    }

    fun onSave() {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                if (!database.getAllCategoryName().contains(category)) database.insertCategory(
                    Category(category)
                )
                val item = database.getOnePantryItem(name)
                item.amount = if (amount.isNotEmpty()) {
                    amount.toLong()
                } else {
                    0L
                }
                item.unit = unit
                item.tags = tags
                item.comment = comment
                item.categoryName = category
                database.updatePantryItem(item)
            }
        }
    }
}