package com.xlipton.whatsinmypantry.newpantryitem

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.xlipton.whatsinmypantry.R
import com.xlipton.whatsinmypantry.database.MyPantryDatabase
import com.xlipton.whatsinmypantry.databinding.FragmentNewPantryItemBinding

class NewPantryItemFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var viewModel: NewPantryItemViewModel

    // For the spinners onItemSelected()
    private lateinit var unit: String
    private var category = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentNewPantryItemBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_pantry_item, container, false)

        val application = requireNotNull(this.activity).application
        val dataSource = MyPantryDatabase.getInstance(application).myPantryDatabaseDao

        // ViewModel
        val viewModelFactory = NewPantryItemViewModelFactory(dataSource)
        val viewModel = ViewModelProvider(this, viewModelFactory).get(NewPantryItemViewModel::class.java)

        // Unit Spinner
        val unitSpinner: Spinner = binding.unitSpinner
        unitSpinner.onItemSelectedListener = this

        activity?.applicationContext?.let { ArrayAdapter.createFromResource(it, R.array.unit_array, android.R.layout.simple_spinner_item).also{
            arrayAdapter -> arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            unitSpinner.adapter = arrayAdapter
        }}
        // Unit Spinner END

        // Category Spinner
        val categorySpinner: Spinner = binding.categorySpinner
        categorySpinner.onItemSelectedListener = this

        viewModel.nameOfCategoriesLive.observe(viewLifecycleOwner, Observer {
            if(!it.contains("add new")) it.add("add new")
            val arrayAdapter = activity?.applicationContext?.let { context -> ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, it).also{
                arrayAdapter -> arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                categorySpinner.adapter = arrayAdapter
            }}
            //Log.i("categorySpinner", "observing: ${categoryNames[0]}")
        })
        // Category Spinner END

        //Log.i("categorySpinner", "first category's name: ${viewModel.nameOfCategoriesLive}")
        //Log.i("categorySpinner", "first category's name from object: ${viewModel.categories.value?.get(0)?.categoryName}")

        binding.newPantryItemViewModel = viewModel
        binding.lifecycleOwner = this

        // Save event observer
        viewModel.eventSave.observe(viewLifecycleOwner, Observer {
            if(it){
                viewModel.unit = unit
                if (!category.isNullOrEmpty()) viewModel.category = category
                this.findNavController().navigate(R.id.action_newPantryItemFragment_to_pantryFragment)
                viewModel.onSaved()
            }
        })

        // Pop up event observer
        viewModel.eventPopUp.observe(viewLifecycleOwner, Observer {
            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        })
        return binding.root

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id){
            R.id.unit_spinner -> unit = parent.getItemAtPosition(position).toString()
            R.id.category_spinner -> {
                if (parent.getItemAtPosition(position).toString() == "add new") {
                    category = ""
                    // TODO: dialog for adding a new category
                } else{
                    category = parent.getItemAtPosition(position).toString()
                }}
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        when (parent?.id){
            R.id.unit_spinner -> unit = parent.firstVisiblePosition.toString()
            R.id.category_spinner -> category = parent.firstVisiblePosition.toString()
        }
    }
}