package com.xlipton.whatsinmypantry.pantry

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.xlipton.whatsinmypantry.R
import com.xlipton.whatsinmypantry.database.MyPantryDatabase
import com.xlipton.whatsinmypantry.databinding.FragmentPantryBinding
import com.xlipton.whatsinmypantry.databinding.FragmentPantryItemBinding

class PantryItemFragment : Fragment(), AdapterView.OnItemSelectedListener{
    private lateinit var viewModel: PantryViewModel

    // For the spinners onItemSelected()
    private lateinit var unit: String
    private var category = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentPantryItemBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pantry_item, container, false)

        val application = requireNotNull(this.activity).application

        val dataSource = MyPantryDatabase.getInstance(application).myPantryDatabaseDao

        val viewModelFactory = PantryViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this,viewModelFactory).get(PantryViewModel::class.java)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        // Navigate to Pantry Fragment
        binding.pantryButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_pantryItemFragment_to_pantryFragment))

        // Unit Spinner
        val unitSpinner: Spinner = binding.unitSpinner
        unitSpinner.onItemSelectedListener = this

        activity?.applicationContext?.let { ArrayAdapter.createFromResource(it, R.array.unit_array, android.R.layout.simple_spinner_item).also{
            arrayAdapter -> arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            unitSpinner.adapter = arrayAdapter
        }}
        // Unit Spinner END

        // Category Spinner
        val categorySpinner: Spinner = binding.categorySpinner
        categorySpinner.onItemSelectedListener = this

        viewModel.nameOfCategoriesLive.observe(viewLifecycleOwner, Observer {
            if(!it.contains("add new")) it.add("add new")
            val arrayAdapter = activity?.applicationContext?.let { context -> ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, it).also{
                    arrayAdapter -> arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                categorySpinner.adapter = arrayAdapter
            }}
        })
        // Category Spinner END

        // Safe args bundle
        viewModel.onLoad(PantryItemFragmentArgs.fromBundle(requireArguments()).itemName, binding)


        binding.saveButton.setOnClickListener {
            viewModel.onSave()
            this.findNavController().navigate(PantryItemFragmentDirections.actionPantryItemFragmentToPantryFragment())
        }


        return binding.root
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id){
            R.id.unit_spinner -> unit = parent.getItemAtPosition(position).toString()
            R.id.category_spinner -> {
                if (parent.getItemAtPosition(position).toString() == "add new") {
                    category = ""
                    // TODO: dialog for adding a new category
                } else{
                    category = parent.getItemAtPosition(position).toString()
                }}
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        when (parent?.id){
            R.id.unit_spinner -> unit = parent.firstVisiblePosition.toString()
            R.id.category_spinner -> category = parent.firstVisiblePosition.toString()
        }
    }
}