package com.xlipton.whatsinmypantry.database.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation
import com.xlipton.whatsinmypantry.database.entities.Category
import com.xlipton.whatsinmypantry.database.entities.PantryItem

data class CategoryWithPantryItems(
    @Embedded
    val category: Category,

    @Relation(parentColumn = "categoryName", entityColumn = "categoryName")
    val pantryItems: List<PantryItem>
)