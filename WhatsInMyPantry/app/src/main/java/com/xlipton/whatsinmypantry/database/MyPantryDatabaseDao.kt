package com.xlipton.whatsinmypantry.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.xlipton.whatsinmypantry.database.entities.Category
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import com.xlipton.whatsinmypantry.database.entities.CategoryWithPantryItems
import com.xlipton.whatsinmypantry.database.entities.ShoppingListItem

@Dao
interface MyPantryDatabaseDao {
    // Category
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(category: Category)

    @Update
    fun updateCategory(item: Category)

    @Query("SELECT * FROM Category")
    fun getAllCategory(): List<Category>

    @Query("SELECT * FROM Category WHERE categoryName = :categoryName")
    fun getOneCategory(categoryName: String): Category

    @Transaction
    @Query("SELECT * FROM Category")
    fun getAllCategoryLive(): LiveData<List<Category>>

    @Query("SELECT categoryName FROM Category")
    fun getAllCategoryNameLive(): LiveData<MutableList<String>>

    @Query("SELECT categoryName FROM Category")
    fun getAllCategoryName(): List<String>



    //PantryItem
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPantryItem(item: PantryItem)

    @Update
    fun updatePantryItem(item: PantryItem)

    @Query("SELECT * FROM PantryItem")
    fun getAllPantryItem(): List<PantryItem>

    @Query("SELECT * FROM PantryItem WHERE itemName = :itemName")
    fun getOnePantryItem(itemName: String): PantryItem

    @Query("SELECT * FROM PantryItem")
    fun getAllPantryItemLive(): LiveData<List<PantryItem>>

    @Query("SELECT itemName FROM PantryItem")
    fun getAllPantryItemName(): List<String>


    // ShoppingListItem
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShoppingListItem(item: ShoppingListItem)

    @Query("SELECT * FROM ShoppingListItem")
    fun getAllShoppingListItemLive(): LiveData<List<ShoppingListItem>>

    @Query("SELECT * FROM ShoppingListItem WHERE itemId = :itemId")
    fun getOneShoppingListItem(itemId: Long): ShoppingListItem

    @Update
    fun updateShoppingListItem(item: ShoppingListItem)

    @Query("DELETE FROM ShoppingListItem WHERE itemId = :itemId")
    fun deleteOneShoppingListItem(itemId: Long)

    @Query("SELECT * FROM ShoppingListItem WHERE addToPantry = 1")
    fun getAllAddToPantryShoppingListItem(): List<ShoppingListItem>



    // CategoryWithPantryItem
    @Transaction
    @Query("SELECT * FROM Category WHERE categoryName = :categoryName")
    fun getCategoryWithPantryItems(categoryName: String): List<CategoryWithPantryItems>

    @Transaction
    @Query("SELECT * FROM PantryItem INNER JOIN Category on Category.categoryName = PantryItem.categoryName WHERE isSelected = 1 ORDER BY accessCounter")
    fun getPantryItemsWhereCategoryIsSelectedLive(): LiveData<List<PantryItem>>
}