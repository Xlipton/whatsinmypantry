package com.xlipton.whatsinmypantry.newpantryitem

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.xlipton.whatsinmypantry.database.MyPantryDatabaseDao


class NewPantryItemViewModelFactory(private val dataSource: MyPantryDatabaseDao) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NewPantryItemViewModel::class.java)) {
            return NewPantryItemViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}