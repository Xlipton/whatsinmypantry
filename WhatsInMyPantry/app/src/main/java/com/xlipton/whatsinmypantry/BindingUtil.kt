package com.xlipton.whatsinmypantry

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import com.xlipton.whatsinmypantry.database.entities.ShoppingListItem

@BindingAdapter("itemName")
fun TextView.setShoppingListItemNameFormatted(item: ShoppingListItem){
    item.let{
        text = item.name
    }
}

@BindingAdapter("itemAmount")
fun TextView.setShoppingListItemAmountFormatted(item: ShoppingListItem){
    item.let{
        text = item.amount.toString()
    }
}

@BindingAdapter("itemUnit")
fun TextView.setShoppingListItemUnitFormatted(item: ShoppingListItem){
    item.let{
        text = item.unit
    }
}


@BindingAdapter("pantryItemName")
fun TextView.setPantryItemNameFormatted(item: PantryItem){
    item.let{
        text = item.itemName
    }
}

@BindingAdapter("pantryItemAmount")
fun TextView.setPantryItemAmountFormatted(item: PantryItem){
    item.let{
        text = item.amount.toString()
    }
}

@BindingAdapter("pantryItemUnit")
fun TextView.setPantryItemUnitFormatted(item: PantryItem){
    item.let{
        text = item.unit
    }
}

@BindingAdapter("pantryItemCategory")
fun TextView.setPantryItemCategoryFormatted(item: PantryItem){
    item.let{
        text = item.categoryName
    }
}
