package com.xlipton.whatsinmypantry.pantry

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.paris.extensions.style
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.xlipton.whatsinmypantry.R
import com.xlipton.whatsinmypantry.database.MyPantryDatabase
import com.xlipton.whatsinmypantry.databinding.FragmentPantryBinding
import com.xlipton.whatsinmypantry.shoppinglist.ShoppingListItemAdapter
import com.xlipton.whatsinmypantry.shoppinglist.ShoppingListItemListener

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class PantryFragment : Fragment() {

    private lateinit var viewModel: PantryViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentPantryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pantry, container, false)

        val application = requireNotNull(this.activity).application

        val dataSource = MyPantryDatabase.getInstance(application).myPantryDatabaseDao

        val viewModelFactory = PantryViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this,viewModelFactory).get(PantryViewModel::class.java)

        binding.lifecycleOwner = this
        binding.pantryViewModel = viewModel

        binding.fab.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_pantryFragment_to_newPantryItemFragment))
        binding.shoppingListButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_pantryFragment_to_shoppingListFragment))

        // Fill up chip group
        viewModel.categories.observe(viewLifecycleOwner, Observer {
            val chipGroup = binding.pantryCategoryChipGroup
            val chip: Chip
            chipGroup.removeAllViews()
            it.forEach { category ->
                val chip = layoutInflater.inflate(R.layout.category_chip_layout, chipGroup, false) as Chip
                chip.text = category.categoryName
                chip.id = category.categoryId.toInt()
                chip.isChecked = category.isSelected
                chip.setOnClickListener { viewModel.onChipClick(chip.text as String) }
                chipGroup.addView(chip)
            }
        })

        // Recycle view stuff
        val adapter = PantryItemAdapter(PantryItemListener({ this.findNavController().navigate(PantryFragmentDirections.actionPantryFragmentToPantryItemFragment(it)) }, {viewModel.onStar(it)}))
        binding.shoppingListItemList.adapter = adapter
        viewModel.pantryItems.observe(viewLifecycleOwner, Observer {
            it?.let{
                adapter.submitList(it)
            }
        })

        binding.shoppingListItemList.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        // Recycle view stuff END

        // Pop up event observer
        viewModel.eventPopUp.observe(viewLifecycleOwner, Observer {
            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        })


        viewModel.navigationToPantryItems.observe(viewLifecycleOwner, Observer {
            Log.i("Navigation", "Observation")
            it?.let{
                this.findNavController().navigate(PantryFragmentDirections.actionPantryFragmentToPantryItemFragment(it.itemName))
                viewModel.doneToPantryItemNavigating()
            }
        })



        return binding.root
    }
}