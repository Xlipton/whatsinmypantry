package com.xlipton.whatsinmypantry.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.xlipton.whatsinmypantry.database.entities.Category
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import com.xlipton.whatsinmypantry.database.entities.ShoppingListItem

@Database(entities = [PantryItem::class, Category::class, ShoppingListItem::class], version = 7, exportSchema = false)
abstract class MyPantryDatabase : RoomDatabase() {

    abstract val myPantryDatabaseDao: MyPantryDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: MyPantryDatabase? = null

        fun getInstance(context: Context): MyPantryDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MyPantryDatabase::class.java,
                        "my_pantry_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}