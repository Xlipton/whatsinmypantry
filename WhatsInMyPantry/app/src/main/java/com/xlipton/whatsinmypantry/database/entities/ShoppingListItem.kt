package com.xlipton.whatsinmypantry.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ShoppingListItem(
    var name: String,
    @PrimaryKey(autoGenerate = true)
    var itemId: Long = 0L,

    var amount: Long = 0L,

    var unit: String = "",

    var addToPantry: Boolean = false,
)
