package com.xlipton.whatsinmypantry.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(
    val categoryName: String,

    @PrimaryKey(autoGenerate = true)
    val categoryId: Long = 0L,

    var isSelected: Boolean = true
)