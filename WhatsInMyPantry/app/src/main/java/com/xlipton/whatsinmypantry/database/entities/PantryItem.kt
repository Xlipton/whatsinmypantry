package com.xlipton.whatsinmypantry.database.entities

import androidx.room.*

@Entity
data class PantryItem(
    @PrimaryKey(autoGenerate = false)
    val itemName: String,

    var categoryName: String,

    var amount: Long = 0L,

    var unit: String = "",

    var accessCounter: Int = 0,

    var isFavourite: Boolean = false,

    var tags: String = "",

    var comment: String = ""
)