package com.xlipton.whatsinmypantry.shoppinglist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.xlipton.whatsinmypantry.database.entities.ShoppingListItem
import com.xlipton.whatsinmypantry.databinding.ListItemShoppingListItemBinding

// and SleepNightAdapter.ViewHolder with RecyclerView.ViewHolder.
class ShoppingListItemAdapter(val clickListener: ShoppingListItemListener) : ListAdapter<ShoppingListItem, ShoppingListItemAdapter.ViewHolder>(ShoppingListDiffCallback()) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener,item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ListItemShoppingListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(clickListener: ShoppingListItemListener, listItem: ShoppingListItem) {
            binding.item = listItem
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemShoppingListItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
             }
        }
    }
}

class ShoppingListDiffCallback : DiffUtil.ItemCallback<ShoppingListItem>() {
    override fun areItemsTheSame(oldItem: ShoppingListItem, newItem: ShoppingListItem): Boolean {
        return oldItem.itemId == newItem.itemId
    }

    override fun areContentsTheSame(oldItem: ShoppingListItem, newItem: ShoppingListItem): Boolean {
        return oldItem == newItem
    }
}

class ShoppingListItemListener(val checkClickListener: (itemId: Long) -> Unit, val deleteClickListener: (itemId: Long) -> Unit) {
    fun onCheckClick(item: ShoppingListItem) = checkClickListener(item.itemId)
    fun onDeleteClick(item: ShoppingListItem) = deleteClickListener(item.itemId)
}
