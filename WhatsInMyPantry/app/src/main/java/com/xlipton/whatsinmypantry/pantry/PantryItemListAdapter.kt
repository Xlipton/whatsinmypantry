package com.xlipton.whatsinmypantry.pantry

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import com.xlipton.whatsinmypantry.databinding.ListItemPantryItemBinding


class PantryItemAdapter(val clickListener: PantryItemListener) : ListAdapter<PantryItem, PantryItemAdapter.ViewHolder>(PantryItemDiffCallback()) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener,item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ListItemPantryItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(clickListener: PantryItemListener, listItem: PantryItem) {
            binding.item = listItem
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemPantryItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class PantryItemDiffCallback : DiffUtil.ItemCallback<PantryItem>() {
    override fun areItemsTheSame(oldItem: PantryItem, newItem: PantryItem): Boolean {
        return oldItem.itemName == newItem.itemName
    }

    override fun areContentsTheSame(oldItem: PantryItem, newItem: PantryItem): Boolean {
        return oldItem == newItem
    }
}

class PantryItemListener(val clickListener: (itemName: String) -> Unit, val starClickListener: (itemName: String) -> Unit) {
    fun onClick(item: PantryItem) = clickListener(item.itemName)
    fun onStarClick(item: PantryItem) = starClickListener(item.itemName)
}
