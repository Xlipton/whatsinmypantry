package com.xlipton.whatsinmypantry

import androidx.room.Room
import com.xlipton.whatsinmypantry.database.MyPantryDatabase
import com.xlipton.whatsinmypantry.database.MyPantryDatabaseDao
import com.xlipton.whatsinmypantry.database.entities.Category
import org.junit.After
import org.junit.Test
//import androidx.test.core.app.ApplicationProvider
import com.xlipton.whatsinmypantry.database.entities.PantryItem
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Before
import java.io.IOException

class MyPantryDatabaseTest {
    private lateinit var myPantryDao: MyPantryDatabaseDao
    private lateinit var databaseMy: MyPantryDatabase

    @Before
    fun createDatabase() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        databaseMy = Room.inMemoryDatabaseBuilder(context, MyPantryDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        myPantryDao = databaseMy.myPantryDatabaseDao
    }

    @After
    @Throws(IOException::class)
    fun closeDatabase() {
        databaseMy.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertCategoryAndGetAll() {
        val category = Category("test")
        myPantryDao.insertCategory(category)
        val getBack = myPantryDao.getAllCategory()
        assertEquals("test", getBack.get(0).categoryName)
    }

    @Test
    @Throws(Exception::class)
    fun insertPantryItemAndGetAll() {
        val item = PantryItem("test")
        myPantryDao.insertPantryItem(item)
        val getBack = myPantryDao.getAllPantryItem()
        assertEquals("test", getBack.get(0).itemName)
    }

    @Test
    @Throws(Exception::class)
    fun pantryItemFieldsTests() {
        val item = PantryItem("test")
        item.amount = 10
        item.categoryName = "test"
        item.clickCounter = 4
        item.comment = "This is a test comment!"
        item.tags = "#meal1#meal2"
        item.unit = "db"
        myPantryDao.insertPantryItem(item)
        val getBack = myPantryDao.getAllPantryItem()
        assertEquals("test", getBack.get(0).itemName)
        assertEquals(10, getBack.get(0).amount)
        assertEquals("test", getBack.get(0).categoryName)
        assertEquals(4, getBack.get(0).clickCounter)
        assertEquals("This is a test comment!", getBack.get(0).comment)
        assertEquals("#meal1#meal2", getBack.get(0).tags)
        assertEquals("db", getBack.get(0).unit)
    }

    @Test
    @Throws(Exception::class)
    fun pantryItemAndCategoryRelation(){
        val category = Category("test")
        myPantryDao.insertCategory(category)
        val getBackCategory = myPantryDao.getAllCategory()

        val item1 = PantryItem("test1")
        item1.categoryName = "test"
        myPantryDao.insertPantryItem(item1)
        //val getBackItem1 = pantryDao.getAllPantryItem()
        val item2 = PantryItem("test2")
        item2.categoryName = "test"
        myPantryDao.insertPantryItem(item2)
        //val getBackItem2 = pantryDao.getAllPantryItem()

        val categoryWithPantryItems = myPantryDao.getCategoryWithPantryItems("test")

        assertEquals("test1", categoryWithPantryItems.get(0).pantryItems.get(0).itemName)
        assertEquals("test2", categoryWithPantryItems.get(0).pantryItems.get(1).itemName)
    }
}